<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* Passando as rotas aqui.

Route::get('/categoria/{idCat}', function ($idCat) {
    return "Posts da categoria {$idCat}";

});

Route::get('/empresa', function () {
    return 'Pg empresa';

});

Route::get('/', function () {
    return view('welcome');
});*/



Route::get('/produto/testes','ProductController@testes');

Route::resource('/produto','ProductController');


Route::get('/contato','SiteController@contato'); 

Route::get('/','SiteController@index'); 

Route::get('/categoria/{id}','SiteController@categoria');

Route::get('/categoria2/{id?}','SiteController@categoria2');