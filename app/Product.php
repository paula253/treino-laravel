<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //Campos que podem ser preenchidos no BD.
    protected $fillable=['name','number','active','category','description'];
    
    //Campos que não podem ser preenchidos.
    //protected $quarted=['admin'];
}
