<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index ()
    {
        $teste1='123';
        $teste2='321';
        $teste3='132';
        
        $var1='123';
        
        $arrayData= [1,2,3,4,5,6,7,8,9];
        
        return view('site.index',compact('teste1','teste2','teste3','var1','arrayData'));
    }

    public function contato ()
    {
        return view('site.contato');
    }


    public function categoria ($id)
    {
        return "Listagem dos postes da caregoria {$id} ";
    }
    
    public function categoria2 ($id='')
    {
        return "Listagem dos postes da caregoria {$id} ";
    }    
    
}    