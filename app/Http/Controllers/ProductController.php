<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// trabalhar com a model Product;
use App\Product; 

//para fazer a validação com request.
use App\Http\Requests\Painel\ProductFormRequest;

class ProductController extends Controller
{
    private $product;
    
    private $totalPage=3;
    
    public function __construct(Product $product)
    {
        $this->products=$product;
    }
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
         $title='Listagem de Produtos';
        
         $products = $this->products->paginate($this->totalPage);  
        
         return view('products',compact('products','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorys=['eletronicos', 'moveis', 'limpeza', 'banho'];
        $title='Cadastrar novo Produto';
        
        
        return view('create-edit',compact('title','categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductFormRequest $request)
    {
        
        //pegar todas as informações do formulário
        //dd($request->all());
        
        //pegar apenas algumas informações do formulário.
        // dd($request->only(['name','number']));
       
       
        //pegar informações do formulário exceto os do array.
        //dd($request->except(['_token','category']));
        
        //pegar apenas um cmapo:
        //dd($request->input('name'));

        //Pega todos os dados dque vem do formulario
        $dataForm=$request->all();
        
        $dataForm['active']= (!isset($dataForm['active']) ) ? 0 : 1 ;


    //  $this->validate($request,$this->products->rules);
     
  /*  $validate=validator($dataForm,$this->products->rules,$messages); 
    
    if($validate->fails())
    {
        return redirect()
        ->route('produtos.create')
        ->withErros($validate)
        ->withInput();
    }
    
    */
     
        //Faz o cadastro.
        $insert=$this->products->create($dataForm);
        
        if($insert)
        {
            return redirect()->route('produto.index');
        }
        
        else
        {
            return redirect()->back;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
       $product = $this->products->find($id);
       
       $title="Produto : {$product->name}";
       
       return view('show',compact('title','product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Recuperar o produto pelo id
        $product=$this->products->find($id);
       
        $categorys=['eletronicos', 'moveis', 'limpeza', 'banho'];
        
        $title="Editar Produto {$product->name}";
        
        $num=0;
        
        foreach($categorys as $categoria)
        {

            if($categoria==$product->category)
            {
                return view('create-edit',compact('title','categorys','product','num'));                   
            }
            $num++;            
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductFormRequest $request, $id)
    {
        //recupera todos os dados do formulário
        $dataForm=$request->all();
        
        $dataForm['category']=$dataForm['category']+1;
        

        //recupera o item do banco de dados para editar.
        $products=$this->products->find($id);
        
        //verifica se o produto está ativo.
        $dataForm['active']= (!isset($dataForm['active']) ) ? 0 : 1 ;
        
        //Altera os itens do BD
        $update=$products->update($dataForm);
        
        //Verifica se realmente editou.
        if($update)
        {
            return redirect()->route("produto.index");
        }
        else
        {
            return redirect()->route('produto.edit',$id)->with(['errors'=>"Falha ao editar"]);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=$this->products->find($id);
        
        $delete=$product->delete();
        
        if($delete)
        {
            return(redirect()->route('produto.index'));
        }
        else
        {
            return(redirect()->route('produto.show',$id)->with(['errors'=>"Falha ao deletar"]));
        }
       
    }
    
    public function testes()
    {
        
            
/*--------------------Inserir informações No BD-------------------------------------------
       
        $prod = $this->products;
        $prod->name='Nome do produto';
        $prod->number=13131;
        $prod->active=true;
        $prod->category='eletronicos';
        $prod->description='Descrição do produto';
        
        $insert=$prod->save();

        if($insert)
        {
            return "Inserido com sucesso";
        }
        else 
        {
            return "Falha ao inserir";
        }
        
        
        */
        
      /*  $insert=$this->products->insert([
            
                'name'=>'Nome do Produto 2',
                'number'=>123,
                'active'=>false,
                'category'=>'eletronicos',
                'description'=>'Descrição do produto aqui',
            ]);
            
        if($insert)
        {
            return "Inserido com sucesso";
        }
        else 
        {
            return "Falha ao inserir";
        }        
        */

        //Especificar na model Products quais campos devem ser preenchidos,quando passar um array.
       /* $insert=$this->products->create([
            
                'name'=>'Nome do Produto 2',
                'number'=>123,
                'active'=>false,
                'category'=>'eletronicos',
                'description'=>'Descrição do produto aqui',
            ]);
            
        if($insert)
        {
            return "Inserido com sucesso ID: {$insert->id}";
        }
        else 
        {
            return "Falha ao inserir";
        }  
        
---------------------------------------------------------------------------------------------*/
/* Testes para atualizar informações ---------------------------------------------------------
    $prod=$this->products->find(5);
    
    /*Utlilzar como debug:
                            dd($prod->name); */
                            
                            
    /*    $prod->name='Nome do produto update';
        $prod->number=789;
        $prod->active=true;
        $prod->category='eletronicos';
        $prod->description='Descr update';
        
        $update=$prod->save();
   
        if($update)
        {
            return("Produto modificado !");
        }
        else
        {
            return("Erro ao modificar o produto");
        }
        
        */
        
        
        //Precisa colocar quais tabela devem sofrer alterações no Model.
       
       /* $prod=$this->products->find(6);
        $update=$prod->update([
            
                'name'=>'Update teste',
                'number'=>7658,
                'active'=>true,
            ]);
            
        if($update)
        {
            return("Produto modificado !");
        }
        else
        {
            return("Erro ao modificar o produto");
        }           
        */
        
        /*prod=$this->products->where('number','=',1234);
        $update=$prod->update([
            
                'name'=>'Update teste 2',
                'number'=>12340,
                'active'=>false,
            ]);
            
        if($update)
        {
            return("Produto modificado 2 !");
        }
        else
        {
            return("Erro ao modificar o produto");
        }           
            
----------------------------------------------------------------------------------------------*/

/*$prod=$this->products->find(3);
        
        $delete=$prod->delete();
        
        if($delete)
        {
            return('Sucesso ao deletar o item');
        }
        else
        {
            return("Erro au deletar o intem");
        }*/
        
    /*    
        
        //posso passar um array para deletar uns intes        
         $prod=$this->products->destroy(5);
       
    */
    
        $delete=$this->products->where('number','=',12340)->delete();
        
        if($delete)
        {
            return('Sucesso ao deletar o item 2 ');
        }
        else
        {
            return("Erro au deletar o intem");
        }  
    
    

    }
}
