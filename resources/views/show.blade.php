@extends('templates.template')

@section('content')

<H1 class="title-pg">    <h1 class='title-pg'><a href="{{route('produto.index')}}"><span class='glyphicon glyphicon-fast-backward'></span> </a>Produto : <b>{{$product->name}}</b></H1>
    
   <p><b>Ativo:</b>{{$product->active}}</p> 
   <p><b>Número:</b>{{$product->number}}</p>     
   <p><b>Categoria:</b>{{$product->category}}</p> 
   <p><b>Descrição:</b>{{$product->description}}</p> 
   
   <hr>
   <!--Caso ocorra algum erro -->
    @if(isset($errors) && count($errors) > 0)
        <div class='alert alert-danger'>
            @foreach($errors->all() as $error)
                <p>{{$error}}</p>
            @endforeach
        </div>
    @endif   
   
   
   {!! Form::open(['route'=>['produto.destroy',$product->id], 'method'=>'DELETE']) !!}
   
    {!! Form::submit("Deletar Produto $product->name",['class'=>'btn btn-danger']) !!}
   
   {!! Form::close() !!}
       
@endsection