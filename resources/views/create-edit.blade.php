@extends('templates.template')


@section('content')

    <h1 class='title-pg'><a href="{{route('produto.index')}}"><span class='glyphicon glyphicon-fast-backward'></span> </a><b>Gestão Produto: {{$product->name  or 'Novo'}}</b></h1>

    <!--Mostrar quais campos ocorreram erros -->
    @if(isset($errors) && count($errors) > 0)
        <div class='alert alert-danger'>
            @foreach($errors->all() as $error)
                <p>{{$error}}</p>
            @endforeach
        </div>
    @endif
    <!---------------------------------------->


    <!--Se existe: É para editar-->    
    @if (isset($product))
            {!! Form::model($product,['route'=>['produto.update',$product->id],'class'=>'form','method'=>'put']) !!}
            
        @else
            {!! Form::open(['route'=>'produto.store','class'=>'form']) !!}
            
    @endif    
    
        <div class='form-group'>
            {!! Form::text('name',null,['class' =>'form-control','placeholder'=>'Nome:']) !!}
        </div>
        
        <div class='form-group'>
            <label>
                {!! Form::checkbox('active') !!}
                Ativo?
            </label>    
        </div>
            
        <div class='form-group'>
            {!! Form::text('number',null,['class' =>'form-control','placeholder'=>'Número:']) !!}            
        </div>
        
     
        @if (isset($product))

                <div class='form-group'>
        
                     {!! Form::select('category',$categorys,$num,['placeholder'=>'Escolha uma categoria','class'=>'form-control']) !!}   
                </div>
                
            @else
                
            <div class='form-group'>
                 {!! Form::select('category',$categorys,null,['placeholder'=>'Escolha uma categoria','class'=>'form-control']) !!}   
            </div>
    @endif   

        
        <div class='form-group'>
            {!! Form::textarea('description',null,['class' =>'form-control','placeholder'=>'Descrição:']) !!}
        </div>
        
        {!! Form::submit('Enviar',['class'=>'btn btn-primary']) !!}
    
    {!! Form::close() !!}
    
@endsection