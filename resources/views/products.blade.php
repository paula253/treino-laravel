@extends('templates.template')

@section('content')

    <H1 class="title-pg">Listagem dos produtos</H1>
    
    <!--<a href="{{url('produto/create')}}" class="btn btn-primary btn-add"><span class="glyphicon glyphicon-plus"></span> Cadastrar</a>-->
    <a href="{{route('produto.create')}}" class="btn btn-primary btn-add"><span class="glyphicon glyphicon-plus"></span> Cadastrar</a>
    
    
    <table class="table table-striped" >
        <tr>
            <th>Nome</th>
            <th>Descrição</th>
            <th widtb='300px'>Ações</th>
        </tr>
            
        @foreach($products as $produto)
            <tr>
                <td>{{$produto->name}}</td>
                <td>{{$produto->description}}</td>
                <td>
                    <a href="{{route('produto.edit',$produto->id)}}" class="edit actions"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="{{route('produto.show',$produto->id)}}" class="delete actions"><span class="glyphicon glyphicon-eye-open"></span></a>
                </td>
            </tr>
        @endforeach
        
    </table>
    
    
    {!! $products->links() !!}

@endsection